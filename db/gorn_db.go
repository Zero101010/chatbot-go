package db

import (
	"fmt"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

type Users struct {
	Name string
	Age  int
}

func ConnectGorn() {
	dbConnection := "root:12345678@tcp(127.0.0.1:3307)/bot-db"
	db, err := gorm.Open(mysql.Open(dbConnection), &gorm.Config{})
	sqlDB, err := db.DB()
	if err != nil {
		fmt.Println(err, "Unable to access the database!")
	}
	fmt.Println("Conectou")
	db.AutoMigrate(&Users{})
	user := Users{Name: "Jinzhu", Age: 18}
	result := db.Create(&user)
	fmt.Println(result.RowsAffected)
	defer sqlDB.Close()
}

// func Migrate()
