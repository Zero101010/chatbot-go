package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

type User struct {
	id        uint64
	firstName string
	lastName  string
}
type Quote struct {
	id       uint64
	name     string
	value    float64
	quantity int64
	user
}

type responseData struct {
	Ask     float64 `json:"ask"`
	DayHigh float64 `json:"dayHigh"`
	DayLow  float64 `json: "dayLow"`
	Open    float64 `json: "open"`
}

func getURL(url string) []byte {
	resp, err := http.Get(url)
	if err != nil {
		log.Fatalln(err)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}
	return body
}

var token = os.Getenv("TOKEN")

func main() {
	// conecta com o bot do telegram
	bot, err := tgbotapi.NewBotAPI(token)
	if err != nil {
		log.Fatalln(err)
	}

	bot.Debug = true

	log.Printf("Authorized on account %s", bot.Self.UserName)

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates, err := bot.GetUpdatesChan(u)

	for update := range updates {
		if update.Message == nil { // ignore any non-Message Updates
			continue
		}

		log.Printf("[%s] %s", update.Message.From.UserName, update.Message.Text)
		if strings.Contains(update.Message.Text, "/price") {
			secondWord := strings.Fields(update.Message.Text)[1]
			body := getURL("http://localhost:5000/price/" + secondWord)
			var respBody responseData
			json.Unmarshal(body, &respBody)
			// Convertendo valores recebidos pela api
			ask := fmt.Sprintf("%.2f", respBody.Ask)
			dayHigh := fmt.Sprintf("%.2f", respBody.DayHigh)
			dayLow := fmt.Sprintf("%.2f", respBody.DayLow)
			open := fmt.Sprintf("%.2f", respBody.Open)

			msg := tgbotapi.NewMessage(update.Message.Chat.ID, "Valor da Ação: "+ask+" \nAlta do Dia: "+dayHigh+"\nBaixa do Dia: "+dayLow+" \nAbertura: "+open)
			bot.Send(msg)
		} else if strings.Contains(update.Message.Text, "/add_quote") {
			quote := strings.Fields(update.Message.Text)[1]
			valueQuote := strings.Fields(update.Message.Text)[2]
			quantityQuote := strings.Fields(update.Message.Text)[3]
			fmt.Println(quote)
			fmt.Println(valueQuote)
			fmt.Println(quantityQuote)
			fmt.Println(update.Message.Chat.ID)
		}

		// retorna a última mensagem
		//msg.ReplyToMessageID = update.Message.MessageID
		//msg.ReplyToMessageID = update.Message.MessageID
	}
}
