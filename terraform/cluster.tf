resource "google_container_cluster" "primary" {

  name               =  var.project_name
  location           = var.region
  initial_node_count = var.node_count

  master_auth {
    username = ""
    password = ""

    client_certificate_config {
      issue_client_certificate = false
    }
  }

  node_config {
    oauth_scopes = [
      "https://www.googleapis.com/auth/cloud-platform"
    ]
    machine_type = "e2-small"

    metadata = {
      disable-legacy-endpoints = "true"
    }
     labels = {
      app = var.project_name
    }

    tags = ["app", var.project_name]

  }
  network_policy {
    enabled = true
  }

  timeouts {
    create = "50m"
    update = "50m"
  }
  provisioner "local-exec" {
    command = "gcloud container clusters get-credentials cluster-terraform --zone us-central1-a --project terraform-297904"
  }
}

