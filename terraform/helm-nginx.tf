resource "helm_release" "nginx-teste" {
  name       = "nginx-teste"
  chart      = "./nginx"


  set {
        name  = "release.name"
        value = "nginx"
    }
  set {
        name  = "release.image"
        value = "nginx"
    }
  set {
        name  = "release.port"
        value = "80"
    }
  depends_on = [google_container_cluster.primary]
}
