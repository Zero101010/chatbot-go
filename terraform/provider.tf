provider "google" {
  version = "3.5.0"
  credentials = file("key_service_count.json")
  project = "terraform-297904"
  zone = "us-central1-c"
}
