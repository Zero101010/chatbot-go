from yahooquery import Ticker
# get data of 
def get_data(data):
    df_request = Ticker("{}.SA".format(data))
    return df_request

def actual_data(data):
    df = get_data(data)
    response = df.summary_detail
    return response

def history_data(data):
    df = get_data(data)
    df = df_request.history(period="1d",  interval = "1m")
    return df

from flask import Flask
from flask import jsonify
app = Flask(__name__)

@app.route('/price/<name>')
def price(name):
    json_response = actual_data(name)
    response_quote = json_response["{}.SA".format(name)]
    return response_quote
